---
title: "FinalProjectScript"
output: html_document
Name: Sandeep Adluri, Ramya Avula, Tejasvi Adepu
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
#Name: Sandeep Adluri, Ramya Deepthi Avula, Tejasvi Adepu
#Title: "FinalProjectScript"
## R Markdown
# Data Loading
```{r}
#Loading data using stringsAsFactors=T to check summary of numeric and factor variables
setwd("/Users/sandeepadluri/Desktop/IS6489-Stats/FinalProject/FinalFolder/")
train <- read.csv(file="train.csv", stringsAsFactors = T)
test <- read.csv(file="test.csv", stringsAsFactors = T)
#Loading data using stringsAsFactors=F
train <- read.csv(file="train.csv", stringsAsFactors = F)
test <- read.csv(file="test.csv", stringsAsFactors = F)
```

# Exploratory Data Analysis- Data Cleaning & Imputation
```{r}
#Combining data before factoring variables so that train and test have same levels.
#This is to avoid error during prediction due to new levels in any features in test dataset.
data <- rbind(train[,-81],test)
str(data) #checking data
data <- data[-1] #Removing Id from data

#Replacing NAs which are not missing values
replacena <- function(x) {
  ifelse(is.na(x),"None",x)
}

#Calling replacena function to replace NAs with missing values
data[,c(6,30,31,32,33,35,57,58,60,63,64,72,73,74)] <- data.frame(lapply(data[,c(6,30,31,32,33,35,57,58,60,63,64,72,73,74)],replacena))

#converting categorical variables which are currently int or character to factor
data[c(1,2,5,6,7,8,9,10,11,12,13,14,15,16,17,18,21,22,23,24,25,27,28,29,30,35,39,40,41,42,53,55,57,60,63,65,72,76,78,79)] <- data.frame(lapply(data[c(1,2,5,6,7,8,9,10,11,12,13,14,15,16,17,18,21,22,23,24,25,27,28,29,30,35,39,40,41,42,53,55,57,60,63,65,72,76,78,79)],as.factor))

#Re-creating train and test data
train <- cbind(data[1:1460,],SalePrice=train$SalePrice)
test <- data[1461:2919,]
```

## Exploratory Data Analysis Continuation - Data Imputation..
```{r}
#Imputing
library(missForest)
set.seed(1234)
trainimp <- train
trainimp[,-80] <- missForest(trainimp[,-80])$ximp
summary(trainimp)

#Test-Imputation
set.seed(1234)
testimp <- missForest(test)$ximp
summary(testimp)


#Refactoring to create columns to help with Modeling

dataimp <- rbind(trainimp[,-80],testimp) #combining data to refactor train & test at a time.

#Removing redundant features
cor(dataimp$TotalBsmtSF,dataimp$BsmtFinSF1+dataimp$BsmtFinSF2+dataimp$BsmtUnfSF)
#Correlation is 0.9999. So keeping just TotalBsmtSF
dataimp <- subset(dataimp,select=-c(BsmtFinSF1,BsmtFinSF2,BsmtUnfSF))

#Removing X1stFlrSF,X2ndFlrSF,LowQualFinSF because these are explained by GrLivArea
cor(dataimp$GrLivArea,dataimp$X1stFlrSF+dataimp$X2ndFlrSF+dataimp$LowQualFinSF) #cor=1
dataimp = subset(dataimp, select=-c(X1stFlrSF,X2ndFlrSF,LowQualFinSF))

#Creating New variables
#combining all bathroom features and removing individual features
dataimp$TotalBath <- dataimp$BsmtFullBath+dataimp$FullBath+0.5*(dataimp$BsmtHalfBath+dataimp$HalfBath)
dataimp = subset(dataimp, select = -c(BsmtHalfBath,BsmtFullBath,HalfBath,FullBath))
#combining all Porch SF+WoodDeckSF  and removing individual features
dataimp$PorchSqFt <- sum(dataimp$WoodDeckSF+dataimp$OpenPorchSF+dataimp$X3SsnPorch+dataimp$ScreenPorch+dataimp$EnclosedPorch)
dataimp <- subset(dataimp,select=-c(WoodDeckSF,OpenPorchSF,X3SsnPorch,ScreenPorch,EnclosedPorch))

#Changinf YearBuilt into categorical variable
#After checking year distribution, year seemed to be better as a categorical variable
#Changing Year to Categorical
dataimp$YearBuiltCat <- ifelse(dataimp$YearBuilt<1981,"Before 1981",
                                ifelse(dataimp$YearBuilt<1991,"1981-1990",
                                       ifelse(dataimp$YearBuilt<2001,"1991-2000","After 2000")))
dataimp$YearBuiltCat <- factor(dataimp$YearBuiltCat)
dataimp <- dataimp[,-19] #Removing original YearBuilt variable
summary(dataimp$YearBuiltCat)

#Breaking dataimp into trainimp and testimp
trainimp <- cbind(dataimp[1:1460,],SalePrice=trainimp$SalePrice)
testimp <- dataimp[1461:2919,]

# Checking skewedness of SalePrice
hist(trainimp$SalePrice)
hist(log(trainimp$SalePrice))

#Removing outliers
plot(trainimp$GrLivArea)
trainimp <- subset(trainimp,GrLivArea <4000)
trainimp=subset(trainimp,LotArea <100000)
trainimp = subset(trainimp, MasVnrArea <1300)

#We have run glmnet model and it reflected variables with near-zero variance
#for most of the levels.
#Removing these
trainimp1 = subset(trainimp, select = -c(Street,Alley,LandContour,Utilities,LandSlope,Condition2,RoofMatl,BsmtCond,
    BsmtFinType2,Heating,Functional,PoolArea,PoolQC, KitchenAbvGr,
                                  MiscFeature,MiscVal,Exterior1st,Exterior2nd,Condition1,HeatingQC,
                                          Electrical,ExterCond,SaleType,RoofStyle,GarageQual,Foundation,PorchSqFt))
```

## Model development
```{r}
library(caret)
set.seed(1234)
tc = trainControl("repeatedcv", number = 10, repeats = 10)

model <-train(log(SalePrice)~.,
              data = trainimp1,
              method = "glmnet",
              preProcess= c("center","scale"),
              trControl = tc)
model
#Predictions
pricePredict <- predict(model, newdata=testimp)
#converting data in to regular scale
salePriceVals <- exp(pricePredict) 
#Writing predictions to a file to submit to Kaggle
submittestData <- read.csv(file="test.csv",stringsAsFactors = F)
submittestData <- cbind(submittestData,salePriceVals) # Dont run multiple times new change
write.csv(data.frame(Id=submittestData$Id,SalePrice=submittestData$salePriceVals),'SubmissionsFinal.csv')

# Kaggle Score = 0.12584
```

## Model Performance Metrics
```{r}
model  #   alpha =1.00   lambda= 0.0005636882  RMSE=0.1185763  R2=0.9101257
#MAE= 0.08342418
(model$results$RMSE)  #  RMSE=0.1185763 
(model$results$Rsquared) #  R2=0.9101257 


#Estimated RMSE & R2 Test
testfinal <- cbind(testimp,SalePrice= salePriceVals)
#Removing outliers in GrLivArea
testfinal <- subset(testfinal,GrLivArea <4000)
#There were variables with near-zero variance for most of the levels.
#Removing these
testfinal = subset(testfinal, select = -c(Street,Alley,LandContour,Utilities,LandSlope,Condition2,RoofMatl,BsmtCond,
    BsmtFinType2,Heating,Functional,PoolArea,PoolQC, KitchenAbvGr,
                                MiscFeature,MiscVal,Exterior1st,Exterior2nd,Condition1,HeatingQC,
                                        Electrical,ExterCond,SaleType,RoofStyle,GarageQual,Foundation,PorchSqFt))

model_Test <-train(log(SalePrice)~.,
              data = testfinal,
              method = "glmnet",
              preProcess= c("center","scale"),
              trControl = tc)
model_Test  #  zero variance on HouseStyle2.5Fin & FenceMnWwThese
# alpha = 0.55  lambda= 0.0005982382  RMSE= 0.01403828  R2= 0.9987720  MAE=0.008686743
(model_Test$results$RMSE)  #  RMSE=0.01403828
(model_Test$results$Rsquared) #  R2= 0.9987720 
```

## Model Top Predictors & Plots
```{r}
varImp(model)
# Final Short list after running the varimp function 
#GrLivArea,MSZoning,TotalBsmtSF,OverallQual,TotalBathm,KitchenQual,Neighborhood,YearBuiltCat

library(tidyverse)

ggplot(trainimp1, aes(GrLivArea, (SalePrice))) +
  geom_point() +
  geom_smooth(method = "lm", se = F) +
  labs(title = "Relationship between  SalePrice and GrlivArea") +
  xlab("Gr Liv Area") +
  ylab("Sale Price Value")

ggplot(trainimp1, aes(TotalBsmtSF, (SalePrice))) +
  geom_point() +
  geom_smooth(method = "lm", se = F) +
  labs(title = "Relationship between  SalePrice and TotalBsmtSF") +
  xlab("TotalBsmtSF") +
  ylab("Sale Price Value")
```

